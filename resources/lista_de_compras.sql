
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS akna;
CREATE DATABASE IF NOT EXISTS akna;
USE akna;

CREATE TABLE `lista_de_compras` (
  `id` int(11) NOT NULL,
  `mes` varchar(20) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `quantidade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lista_de_compras`
--
ALTER TABLE `lista_de_compras`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lista_de_compras`
--
ALTER TABLE `lista_de_compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

