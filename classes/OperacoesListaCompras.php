<?php

class OperacoesListaCompras {

    private $dados = array();
    private $correcao = array();

    public function __construct($dados, $correcao)
    {
        $this->correcao = $correcao;

        $this->corrigirListaCompras($dados);
    }

    public function getDados()
    {
        return $this->dados;
    }

    public function setDados($dados)
    {
        $this->dados = $dados;
    }

    public function corrigirListaCompras(&$array)
    {
        foreach ($array as $key => &$value)
        {
            if (is_array($value)) {
                $this->corrigirListaCompras($value);
            }
            else {
                foreach ($this->correcao as $k => $v)
                {
                    if ($key == $k) {
                        $array[$v] = $array[$key];
                        unset($array[$key]);
                        break;
                    }
                }
            }
        }
        $this->dados = $array;
    }

    public function ordenarPorMes($seq)
    {
        $ret = array();

        if(empty($this->dados) || empty($seq)){
            return false;
        }

        foreach ($seq as $key)
        {
            if (!isset($this->dados[$key]))
            {
                continue;
            }
            $ret[$key] = $this->dados[$key];
        }

        $this->dados = $ret;
    }

    public function ordenarPorCategoria()
    {
        foreach ($this->dados as &$arr)
        {
            ksort($arr);
        }
    }

    public function ordenarPorQuantidade(){

        foreach ($this->dados as &$arr ){
            foreach ($arr as $key => &$value){
                uasort($value, array($this, "Decrescente"));
            }
        }
    }

    private function Decrescente($a, $b) {

        if ($a == $b) {
            return 0;
        }
        return ($a > $b) ? -1 : 1;
    }

}

