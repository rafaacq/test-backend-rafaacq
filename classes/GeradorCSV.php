<?php

class GeradorCSV
{
    private $nomeArquivo;
    private $dados;

    public function __construct($nomeArquivo, $dados)
    {
        $this->nomeArquivo = $nomeArquivo;
        $this->dados = $dados;
    }

    public function geraCSV()
    {
       $arquivo = fopen($this->nomeArquivo, 'w');
       foreach ($this->dados as $linha )
       {
           fputcsv($arquivo, $linha);
       }
        echo "Criando csv... \n" ;
        echo "CSV criado \n";
       fclose($arquivo);
    }

}

