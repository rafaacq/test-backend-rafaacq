<?php


class DBConnection
{
    // Database Connection Configuration Parameters
    // array('driver' => 'mysql','host' => '','dbname' => '','username' => '','password' => '')
    protected $_config;
    // Database Connection
    public $dbc;
    /* function __construct
     * Opens the database connection
     * @param $config is an array of database connection parameters
     */
    public function __construct( array $config ) {
        $this->_config = $config;
        $this->getPDOConnection();
    }
    /* Function __destruct
     * Closes the database connection
     */
    public function __destruct() {
        $this->dbc = NULL;
    }
    /* Function getPDOConnection
     * Get a connection to the database using PDO.
     */
    private function getPDOConnection() {
        // Check if the connection is already established
        if ($this->dbc == NULL) {
            // Create the connection
            $dsn = "" .
                $this->_config['driver'] .
                ":host=" . $this->_config['host'] .
                ";dbname=" . $this->_config['dbname'];
            try {
                $this->dbc = new PDO( $dsn, $this->_config[ 'username' ], $this->_config[ 'password' ] );
            } catch( PDOException $e ) {
                echo __LINE__.$e->getMessage();
            }
        }
    }

    public function runInsert($campos, $tabela, $valores){
        $campos_sql= implode(',', array_values($campos));
        //INSERT INTO lista_de_compras (mes, categoria, produto, quantidade) VALUES ('maio', 'limpeza', 'MOP', '100');
        $camposParametros = ':' . implode(', :', array_values($campos)) ;
        foreach ($valores as $key => $value)
        {
            $fieldsData = '\'' . implode('\', \'', array_values($value)) . '\'';
            $query = "INSERT INTO $tabela ($campos_sql) VALUES ($fieldsData)";
            $this->dbc->exec($query);

        }
        echo "Inserindo dados... \n";
        echo "Dados inseridos \n";
    }


}