<?php

require_once ('classes/OperacoesListaCompras.php');
require_once ('classes/GeradorCSV.php');
require_once ('classes/database/config.php');
require_once ('classes/database/DBConnection.php');

//Inclusão dos arquivos e armazenamento dos dados na variável dados
$lista = require_once('lista-de-compras.php');



$correcoes = array('Papel Hignico'     => 'Papel Higiênico',
                    'Brocolis'          => 'Brócolis',
                    'Chocolate ao leit' => 'Chocolate ao leite',
                    'Sabao em po'       => 'Sabão em pó');


$campos = array('mes', 'categoria', 'produto', 'quantidade');

$listaCompras = new OperacoesListaCompras($lista, $correcoes);


$sequencia = array ('janeiro',  'fevereiro',
                    'marco',    'abril',
                    'maio',     'junho',
                    'julho',    'agosto',
                    'setembro', 'outubro',
                    'novembro', 'dezembro');


$listaCompras->ordenarPorMes($sequencia);
$listaCompras->ordenarPorCategoria();
$listaCompras->ordenarPorQuantidade();


$dadosCSV = array();

foreach ($listaCompras->getDados() as $key =>$value) {
    $mes = $key;
    foreach($value as $k=>$v) {
        $categoria = $k;
        foreach ($v as $chave => $valor) {
            $produto = $chave;
            $quantidade = $valor;
            $dadosCSV[] = array($mes,$categoria,$produto,$quantidade);
        }
    }
}


$objetoCSV = new GeradorCSV('compras-do-ano.csv', $dadosCSV);
$objetoCSV->geraCSV();


$database = new DBConnection($dbconfig);

$database->runInsert($campos, 'lista_de_compras' , $dadosCSV);
















